// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDhztwuYgLzuh83HWE0ZUTfjRRniUZxwAY",
    authDomain: "baking-pastry.firebaseapp.com",
    databaseURL: "https://baking-pastry.firebaseio.com",
    projectId: "baking-pastry",
    storageBucket: "baking-pastry.appspot.com",
    messagingSenderId: "653376144701"
  }
};
