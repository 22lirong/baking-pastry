import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-questions-list',
  templateUrl: './questions-list.component.html',
  styleUrls: ['./questions-list.component.css']
})
export class QuestionsListComponent implements OnInit {

  questionsObservable: Observable<any[]>;

  constructor(
    private db: AngularFireDatabase){
  }

  ngOnInit() {
    this.questionsObservable = this.getQuestions('/faqs');
  }

  getQuestions(listPath): Observable<any[]> {
    return this.db.list(listPath).valueChanges();
  }

}

