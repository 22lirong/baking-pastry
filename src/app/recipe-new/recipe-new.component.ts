import { Component, OnInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recipe-new',
  templateUrl: './recipe-new.component.html',
  styleUrls: ['./recipe-new.component.css']
})

export class RecipeNewComponent implements OnInit {

  @ViewChild('myPicField') myPicRef: ElementRef;

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private route: ActivatedRoute) { }

  processUpload(myRecipeForm: NgForm, myPicField: HTMLInputElement) {
    console.log('> process: ', myPicField);
    console.log('> file name: ', myPicField.files[0]);
    console.log('> myPicRef: ', this.myPicRef.nativeElement.files[0]);

    const uploadData = new FormData();
    uploadData.set('myPic', this.myPicRef.nativeElement.files[0]);
    uploadData.set('recipeName', myRecipeForm.value.recipeName);
    uploadData.set('recipeDescription', myRecipeForm.value.recipeDescription);
    uploadData.set('recipeYield', myRecipeForm.value.recipeYield);
    uploadData.set('recipeUnit', myRecipeForm.value.recipeUnit);
    uploadData.set('recipeCategory', myRecipeForm.value.recipeCategory);
    uploadData.set('recipeSource', myRecipeForm.value.recipeSource);
    
    this.httpClient.post('http://localhost:3000/recipes/unpublished', uploadData)
      .subscribe(
        (result) => {
          console.log('>> OK: ', result);
        }, //good
        (error) => {
          console.error('>> error: ', error);
        }, //bad
        () => {
          console.log('> observable is complete.');
        } //very good
      );
  }

  ngOnInit() {
  }
  
}

