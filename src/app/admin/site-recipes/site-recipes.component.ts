import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-site-recipes',
  templateUrl: './site-recipes.component.html',
  styleUrls: ['./site-recipes.component.css']
})
export class SiteRecipesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
