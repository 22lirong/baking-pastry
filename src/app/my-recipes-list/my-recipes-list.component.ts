import { Component, OnInit, TemplateRef } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-my-recipes-list',
  templateUrl: './my-recipes-list.component.html',
  styleUrls: ['./my-recipes-list.component.css']
})
export class MyRecipesListComponent implements OnInit {

  myRecipesObservable: Observable<any[]>;

  constructor(
    private db: AngularFireDatabase) {
  }

  ngOnInit() {
    this.myRecipesObservable = this.getMyRecipes('/recipes/unpublished');
  }

  getMyRecipes(listPath): Observable<any[]> {
    return this.db.list(listPath).valueChanges();
  }
}
