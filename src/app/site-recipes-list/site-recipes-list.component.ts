import { Component, OnInit, TemplateRef } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-site-recipes-list',
  templateUrl: './site-recipes-list.component.html',
  styleUrls: ['./site-recipes-list.component.css']
})
export class SiteRecipesListComponent implements OnInit {

  siteRecipesObservable: Observable<any[]>;

  constructor(
    private db: AngularFireDatabase) {
  }

  ngOnInit() {
    this.siteRecipesObservable = this.getSiteRecipes('/site-recipes');
  }

  getSiteRecipes(listPath): Observable<any[]> {
    return this.db.list(listPath).valueChanges();
  }

}