import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { rootRouterConfig } from './app.routes';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';

import { UserResolver } from './user/user.resolver';
import { AuthGuard } from './core/auth.guard';
import { AuthService } from './core/auth.service';
import { UserService } from './core/user.service';
import { RecipeService } from './core/recipe.service';

import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { SiteRecipesListComponent } from './site-recipes-list/site-recipes-list.component';
import { AdminComponent } from './admin/admin.component';
import { SiteQuestionsComponent } from './admin/site-questions/site-questions.component';
import { SiteRecipesComponent } from './admin/site-recipes/site-recipes.component';
import { QuestionsListComponent } from './questions-list/questions-list.component';
import { FaqsComponent } from './faqs/faqs.component';
import { MyRecipesComponent } from './my-recipes/my-recipes.component';

import { TabsModule } from 'ngx-bootstrap';
import { SiteQuestionsEditComponent } from './admin/site-questions-edit/site-questions-edit.component';
import { RecipeNewComponent } from './recipe-new/recipe-new.component';
import { MyRecipesListComponent } from './my-recipes-list/my-recipes-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    HomeComponent,
    HeaderComponent,
    SiteRecipesListComponent,
    AdminComponent,
    SiteQuestionsComponent,
    SiteRecipesComponent,
    QuestionsListComponent,
    FaqsComponent,
    MyRecipesComponent,
    SiteQuestionsEditComponent,
    RecipeNewComponent,
    MyRecipesListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: false }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    TabsModule.forRoot(),
    HttpModule,
    HttpClientModule
  ],
  providers: [ AuthService, UserService, UserResolver, AuthGuard, RecipeService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
