import { Injectable, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';

import { Observable, Subject } from 'rxjs/Rx';

import { Recipe } from '../core/recipe.model';
import { Ingredient } from '../core/ingredient.model';

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();
  recipeSelected = new EventEmitter<Recipe>();

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private route: ActivatedRoute) { }
  
  getAllRecipes() {
    this.httpClient.get('http://localhost:3000/recipes/unpublished')
      .subscribe(
        (result) => {
          console.log('>> OK: ', result);
        }, //good
        (error) => {
          console.error('>> error: ', error);
        }, //bad
        () => {
          console.log('> observable is complete.');
        } //very good
      );
  }

}
