# Recipes
1. recipeID: string, system generated
2. recipeName: string
3. recipeDesc: string
4. recipeCat: ["bread", "cake", "", "", "", "", "", ""]
5. recipeSource: optional, string
6. yieldQty: optional, decimal, >= zero, default: 0.0
7. yieldUnit: optional, string, in ["grams", "ml","servings"], default: "grams"
8. urlImage:
9. ingredient: [qty, unit, name]
10. method: [step, instruction]
11. dateCreated:
12. dateUpdated:
13. createdBy: user email or user ID
14. published: true/false, default: false

# Firebase User Object
1. unique ID
2. email address
3. name
4. photo URL

# Role-based Firebase Database rules
1. guest - no sign in required.
- able to view shared recipes and FAQs.
2. registered user - sign in with firebase or google.
- able to access 'my recipes' and 'my profile' pages.
- able to submit new recipe, view/edit/delete own recipes.
3. admin - sign in with "admin" role assigned.
- able to access 'Admin' page to manage site contents.
- able to read/write all data.

# BakingPastry

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
