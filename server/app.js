//Load libraries
const path = require('path');
const multer = require('multer');
const cors = require('cors');
const bodyParser = require('body-parser');
const express = require('express');
const admin = require('firebase-admin');
const fs = require('fs');
const moment = require('moment');
const uuidv1 = require('uuid/v1');

const keys = require('./baking-pastry-firebase-adminsdk-x4ma4-fbf5bd1281');

/*
var admin = require("firebase-admin");

var serviceAccount = require("path/to/serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://baking-pastry.firebaseio.com"
});
*/

//configure
admin.initializeApp({
    databaseURL: 'https://baking-pastry.firebaseio.com/',
    credential: admin.credential.cert(keys)
});

const bucket = admin.storage().bucket('baking-pastry.appspot.com');
const upload = multer({ dest: path.join(__dirname, 'tmp')});

const app = express();

//route
app.use(cors());

app.post('/recipes/unpublished', upload.single('myPic'),
    (req, resp) => {
        //req.file
/*
{ fieldname: 'myPic', originalname: 'image.png', encoding: '7bit',
  mimetype: 'image/png', destination: '/opt/tmp/fsf_r1_2018/day27/server/tmp',
  filename: '79914f7dc4b37a806f841f13cf961b25',
  path: '/opt/tmp/fsf_r1_2018/day27/server/tmp/79914f7dc4b37a806f841f13cf961b25',
  size: 57566 }
*/
        console.log('file = ', req.file);
        console.log('recipeName = ', req.body.recipeName);
        console.log('recipeDescription = ', req.body.recipeDescription);
        console.log('recipeYield = ', req.body.recipeYield);
        console.log('recipeUnit = ', req.body.recipeUnit);
        console.log('recipeCategory', req.body.recipeCategory);
        console.log('recipeSource = ', req.body.recipeSource);
        
        const myRecipeId = uuidv1();

        const remoteFile = bucket.file('images/' + req.file.filename + '-' + req.file.originalname);

        fs.createReadStream(req.file.path)
            .pipe(remoteFile.createWriteStream({
                metadata: {
                    contentType: req.file.mimetype,
                    cacheControl: 'public, max-age=31536000',
                    metadata: {
                        myRecipedId: myRecipeId
                    }
                }
            }))
            .on('error', (error) => {
                console.log('error: ', error);
                resp.status(400).json({error: error});
            })
            .on('finish', () => {
                fs.unlink(req.file.path);
                const config = {
                    action: 'read',
                    expires: moment().add(100, 'years')
                };
                remoteFile.getSignedUrl(config)
                    .then((url) => 
                        admin.database().ref('recipes/unpublished')
                            .push(({
                                myRecipeId: myRecipeId,
                                recipeName: req.body.recipeName,
                                recipeDescription: req.body.recipeDescription,
                                recipeYield: req.body.recipeYield,
                                recipeUnit: req.body.recipeUnit,
                                recipeCategory: req.body.recipeCategory,
                                recipeSource: req.body.recipeSource,
                                url: url,
                                dateCreated: new Date().toString(),
                                dateModified: new Date().toString(),
                                published: false
                            }))
                    ).then(result => {
                        resp.status(201).json({message: "uploaded"});
                    }).catch(error => {
                        console.log('error: ', error);
                        resp.status(400).json({error: error});
                    })
            });
    }
);


// list
/*
admin.database().ref('recipes/unpublished')
    .orderByKey()
    .limitToLast(1)
    .on('child_added', (c) => {
        console.log('new child: ', c.toJSON());
});
*/

// update
admin.database().ref('recipes/unpublished')
    .once('value')
    .then(snapshot => {
        snapshot.forEach(d => {
            console.log('key = ', d.key);
            const data = d.val();
            data.dateModified = new Date().toString();
            admin.database().ref('recipes/unpublished/' + d.key)
                .set(data)
    })
});


//Start express
const PORT = 3000;
app.listen(PORT, () => {
    console.log('Application started on port %d', PORT);
});